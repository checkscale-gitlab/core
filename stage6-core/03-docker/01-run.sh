#!/bin/bash -e

if [[ ! -d "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Docker" ]]; then
    mkdir "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Docker"
fi
curl -fsSL https://get.docker.com | sed 's@docker-ce$pkg_version >/dev/null"@docker-ce$pkg_version >/dev/null" || true@g' > "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Docker/get-docker.sh"

export libseccomp2_pack="libseccomp2_2.5.3-2_armhf.deb"

on_chroot << EOF
export DEBIAN_FRONTEND=noninteractive
cd /home/${FIRST_USER_NAME}/Docker/
if [ ! -f docker-install-finished ]
then sh -a get-docker.sh && touch docker-install-finished
fi
apt-get remove python-configparser || true
usermod -aG docker ${FIRST_USER_NAME}
apt-get install -y -qq --no-install-recommends docker-compose

# patch for alpine:3.13 containers which need a libseccomp2 version greater than 2.4
wget http://ftp.us.debian.org/debian/pool/main/libs/libseccomp/${libseccomp2_pack}
dpkg -i --refuse-downgrade ${libseccomp2_pack}
rm ${libseccomp2_pack}

EOF
