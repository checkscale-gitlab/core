#!/bin/bash

pushd REPLACE_WITH_SERVICES_DIRECTORY

if [[ -z "$1" ]]; then
	# take newest backup
	timestamp=$(ls "Backup" | sort | head -n 1)
	if [[ -z "${timestamp}" ]]; then
		echo "No backups found in the Backup folder"
		exit 1
	fi
	echo "Picking newest backup from ${timestamp}"
	dirname="Backup/${timestamp}"
else
	# take backup given by argument
	if [[ -d "Backup/$1" ]]; then
		echo "Using backup from $1"
		dirname="Backup/$1"
	else
		echo "Specified backup does not exist"
		exit 1
	fi
fi

pushd Generated
docker-compose stop
popd

for backup in $(find ${dirname} -maxdepth 1 -name '*.tar.gz' -printf '%f\n'); do
	volname=$(echo ${backup%.tar.gz})
	docker volume create ${volname}
	docker run --rm -v ${volname}:/recover -v `pwd`/${dirname}:/backup ubuntu bash -c "cd /recover && tar xzmf /backup/${backup}"
done

pushd Generated
docker-compose start
popd

popd