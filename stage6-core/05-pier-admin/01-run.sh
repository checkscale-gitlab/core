#!/bin/bash -e

# new version of the script based on websocketd
PIER_HOME=/home/${FIRST_USER_NAME}
RPIER_HOME="${ROOTFS_DIR}${PIER_HOME}"
ADM_DIR="${PIER_HOME}/Admin"
RADM_DIR="${ROOTFS_DIR}${ADM_DIR}"
mkdir -p "${RADM_DIR}"

SERVICES_DIR=${PIER_HOME}/Services
GENERATED_DIR=${SERVICES_DIR}/Generated

WSD_DIR=${ADM_DIR}/websocketd
RWSD_DIR=${RADM_DIR}/websocketd
mkdir -p ${RWSD_DIR}

SECRETS_DIR=${PIER_HOME}/Services/Secrets
RSECRETS_DIR=${ROOTFS_DIR}/${SECRETS_DIR}

mkdir -p ${RSECRETS_DIR}  # make sure it exists at boot time

#1. Install websocketd (https://github.com/joewalnes/websocketd), a websocket daemon.
  # figure out latest websocketd URL 
LATEST_WSD_VER=$(curl -s https://api.github.com/repos/joewalnes/websocketd/releases/latest | grep "tag_name" | awk '{print substr($2, 3, length($2)-4)}') 
LATEST_WSD_URL="https://github.com/joewalnes/websocketd/releases/download/v${LATEST_WSD_VER}/websocketd-${LATEST_WSD_VER}-linux_arm.zip"
TMPZIP=$(mktemp --suffix .zip)
TMPWSD=$(mktemp -d)
curl -o ${TMPZIP} -L --retry 5 ${LATEST_WSD_URL};
unzip -o -d ${TMPWSD} ${TMPZIP} websocketd
rm ${TMPZIP}
install -m 755 ${TMPWSD}/websocketd "${RWSD_DIR}/websocketd"
rm -rf ${TMPZIP} ${TMPWSD}

# the server that will be run by the daemon
install -m 755 files/websocket-server.py "${RWSD_DIR}/websocket-server.py"
sed -i "s|REPLACE_WITH_WSD_DIRECTORY|${ADM_DIR}/websocketd|g" "${RWSD_DIR}/websocket-server.py"
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|${SERVICES_DIR}|g" "${RWSD_DIR}/websocket-server.py"

# the server config
install -m 644 files/config.json "${RWSD_DIR}/config.json"
sed -i "s|REPLACE_WITH_WSD_DIRECTORY|${WSD_DIR}|g" "${RWSD_DIR}/config.json"
sed -i "s|REPLACE_WITH_SECRETS_DIR|${SECRETS_DIR}|g" "${RWSD_DIR}/config.json"

# install the service and the script that will run the daemon
install -m 644 files/websocketd.service "${ROOTFS_DIR}/etc/systemd/system/websocketd.service"
sed -i "s|REPLACE_WITH_ADMIN_DIRECTORY|${ADM_DIR}|g" "${ROOTFS_DIR}/etc/systemd/system/websocketd.service"

install -m 755 files/start-websocketd.sh "${RADM_DIR}/start-websocketd.sh"
sed -i "s|REPLACE_WITH_WSD_DIRECTORY|${WSD_DIR}|g" "${RADM_DIR}/start-websocketd.sh"

install -m 755 files/update-system.sh "${RADM_DIR}/update-system.sh"

log "enabling service for websocketd"
on_chroot << EOF
  systemctl enable websocketd
EOF
