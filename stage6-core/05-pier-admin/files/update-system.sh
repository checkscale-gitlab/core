#!/bin/bash

echo "Updating $(hostname) pier host operating system..."
apt-get update
apt-get upgrade -y 
#apt-get dist-upgrade -y
apt-get autoremove
# refresh snap if installed
[ ! -z "$(command -v snap)" ] && snap refresh
echo "Updating $(hostname) pier operating system...done"
