#!/bin/bash -e

# clone services repo
# make sure the submodule is initialized and up to date
git submodule init
REMOTE=""
[ "$PIER_ENV" == "development" ] && REMOTE="--remote"
git submodule update $REMOTE

SUB_STAGE_DIR=$(pwd) python3 files/clone_services_repo.py
remote_url=$(cd files/pier-services && git remote get-url origin)

# install dependencies
on_chroot << EOF
set -x
pushd /home/${FIRST_USER_NAME}/Services/pier-services
git remote set-url origin "$remote_url"
pip3 install --retries 3 --no-input -r requirements.txt
popd
EOF
