import sys
sys.path.append("files/pier-services")
import git_repo_utils
import os

# Get the needed environment variables
root_fs_dir = os.environ.get("ROOTFS_DIR")
first_user_name = os.environ.get("FIRST_USER_NAME")
sub_stage_dir = os.environ.get("SUB_STAGE_DIR")
dest_folder = f'{root_fs_dir}/home/{first_user_name}/Services'
repo_url = f'{sub_stage_dir}/files/pier-services'

# The branch and commit are dictated by the submodule
git_repo_utils.clone_repo_in_folder(repo_url=repo_url, dest_folder=dest_folder, branch = None, commit = None)