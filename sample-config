# ===== Pi-Gen important config options, see pi-gen_README.md for more options =====
# required, sets the name of the generated image
IMG_NAME=pier

# optional, sets the hostname of your Pi
# TARGET_HOSTNAME=raspberrypi

# optional, sets the username of the first user
# FIRST_USER_NAME=pi

# optional, sets the password of the first user
# FIRST_USER_PASS=raspberry



# ===== Pier config options =====
# required, is used as endpoint URL/IP for the Wireguard configuration and as domain name for your web services
#           Set to either a domain pointing to your Pi, or to your static public IP address
#           If you have neither a domain or a static public IP address, you can use a free dynamic DNS provider
export DEVICE_DNS='example.com'

# optional, used for setting up of a static local IP your Pi requests on your network. As DEVICE_IP, choose an IP that your router will not give out to other devices on your network. As ROUTER_IP, provide the local IP of your router.
# export DEVICE_IP='192.168.0.11'
# export ROUTER_IP='192.168.0.1'

# optional, set to a path to your SSH **public** key. If set, ssh is enabled and your public key is authorized.
# ENABLE_SSH='/path/to/your/ssh_public_key.pub'

# optional, set to a path to a file containing Wireguard client configuration fragments to pre-initialize as authorized VPN clients
# export WG_CLIENTS='/path/to/your/clients-fragments.conf'

# optional, set to a path of a Wireguard private key to use on the server
# export WG_SERVER_KEY='/path/to/server_private.key'

# optional, set to true if you have a domain name, and wish to expose your services over HTTPS to the public web
#           if unset, services use HTTP and no certificates are generated. We do not recommend exposing the services that way
# export USE_TLS=true

# optional, but highly encouraged. Set to your E-Mail address to be notified about expiring certificates (if auto-extend does not work)
# export CERT_EMAIL='ex@example.com'

# optional, set to the desired way to fulfill an ACME certificate challenge
#           'http' (default):   Needs to have Port 80 exposed
#           'tls':              Needs to have Port 443 exposed
#           'dns':              Needs no exposed ports, but requires manual modification of your DNS record. Run `docker logs traefik` to find instructions
# export ACME_CHALLENGE='http'

# PIER environment (development|production)
# export PIER_ENV=production
export PIER_ENV=development
